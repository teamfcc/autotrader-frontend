import { Component, OnInit } from '@angular/core';
import { SubmissionService } from './submission.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'Itic Autotrader';
  submitFlag = true
  nameStock:String="AAPL";
  sizeTrade:number=1;
  margin:number=1;
  trades:Object[] = []

  //First build  model, here array. (then go on to iterate over it)
  //In JS objects are denoted by {}
  products = [{desc:'Pots', price:12.99},
              {desc:'Dots', price:3.99},
              {desc:'Spots', price:24.99}]

  //mock trades model, incl data type
  mockTrades:any = []

  //model for our data
  tradesModel:any []

  constructor(private submissionService:SubmissionService){
  }

  // declare listener functions for the click button, inside the component.
  // aka a callback, or a handler function.
  showTrades(){
    console.log(event.target)  
    // return this.submitFlag = false
    //   this.submissionService
    //   .getFeed(this.nameStock, this.sizeTrade, this.margin)
  }
  ngOnInit(){
    // this.submissionService.getMockFeed()
  //   this.submissionService.getRealFeed(this.nameStock,this.sizeTrade,this.margin)
  //   .subscribe((result)=>{console.log(result) this.mockTrades=result})
  // }
  this.submissionService.getData().subscribe( (result)=> {this.trades = result
  console.log(result)}, err=>{
    console.log(err)
  })
  }
}
