import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { getLocaleDateFormat } from '@angular/common';
import { Observable, throwError } from 'rxjs';
import {catchError} from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn:'root'
})

export class SubmissionService {

  constructor(private http: HttpClient) {
    // this.apiUrl= environment.rest_host+'/trade'}
  }
  
// access mock json data 
//   no need for parameters as its static data
//   getMockFeed(){
//     return this.http.get("assets/trades.json")
  

// this needs updated with proper URL to access our data feed url
// getRealFeed(nameStock,sizeTrade,margin){
//   return this.http.get(`http://itic.dev2.conygre.com/trade.csv?stockTicker=${this.nameStock}&size=${this.sizeTrade}&extitProfitLoss=${this.margin}`)
//   //return this.http.get<Object[]>(this.apiUrl)
// }

  apiUrl:string = 'http://localhost:8081/trade'
  getData():Observable<Object[]>{
    return this.http.get<Object[]>('http://localhost:8081/trade').pipe(
      catchError(this.handleError<Object[]>('getData', []))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // Let the app keep running by returning an empty result.
      return throwError(
        'Something bad happened; please try again later.')
    };
  }
}